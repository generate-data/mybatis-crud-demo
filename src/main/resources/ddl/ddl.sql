CREATE USER dasreda WITH PASSWORD 'dasreda';

ALTER ROLE dasreda createdb;

CREATE DATABASE "dasreda"
    WITH
    OWNER = dasreda
    ENCODING = 'UTF8'
    LC_COLLATE = 'Russian_Russia.1251'
    LC_CTYPE = 'Russian_Russia.1251'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

COMMENT ON DATABASE "dasreda"
    IS 'Деловая среда';