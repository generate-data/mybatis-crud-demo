package ru.demo.mybatis.mybatisdemo.service;

import ru.demo.mybatis.mybatisdemo.entity.Employee;

import java.util.List;

public interface EmployeeService {
    List<Employee> getEmployees();

    Integer createNewEmployee(Employee employee);

    Employee getEmployeeById(Integer id);

    void updateEmployee(Employee employee);

    void deleteEmployee(Integer id);
}
