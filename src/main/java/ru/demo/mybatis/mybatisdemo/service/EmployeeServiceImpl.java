package ru.demo.mybatis.mybatisdemo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.demo.mybatis.mybatisdemo.entity.Employee;
import ru.demo.mybatis.mybatisdemo.mapper.EmployeeMapper;

import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService{

    private EmployeeMapper employeeMapper;

    @Autowired
    public EmployeeServiceImpl(EmployeeMapper employeeMapper) {
        this.employeeMapper = employeeMapper;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Employee> getEmployees() {
        return employeeMapper.findAll();
    }

    @Override
    public Integer createNewEmployee(Employee employee) {
        return employeeMapper.saveEmployee(employee);
    }

    @Override
    public Employee getEmployeeById(Integer id) {
        return employeeMapper.findById(id);
    }

    @Override
    public void updateEmployee(Employee employee) {
        employeeMapper.saveEmployee(employee);
    }

    @Override
    public void deleteEmployee(Integer id) {
        employeeMapper.deleteEmployee(id);
    }
}
