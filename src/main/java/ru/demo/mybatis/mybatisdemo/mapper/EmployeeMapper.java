package ru.demo.mybatis.mybatisdemo.mapper;

import org.apache.ibatis.annotations.*;
import ru.demo.mybatis.mybatisdemo.entity.Employee;

import java.util.List;

@Mapper
public interface EmployeeMapper {
    @Select("select * from employee")
    List<Employee> findAll();

    @Insert("insert into employee (name, salary) values (#{name}, #{salary})")
    Integer saveEmployee(Employee employee);

    @Update("Update employee set name=#{name} salary=#{salary} where id = #{id}")
    void update(Employee employee);

    @Delete("Delete from employee where id=#{id}")
    public void deleteEmployee(Integer id);

    @Select("select * from employee where id = #{id}")
    Employee findById(Integer id);

}
