package ru.demo.mybatis.mybatisdemo;

import org.apache.ibatis.type.MappedTypes;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ru.demo.mybatis.mybatisdemo.entity.Employee;

@MappedTypes(Employee.class)
@MapperScan("ru.demo.mybatis.mybatisdemo.mapper")
@SpringBootApplication
public class MyBatisDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyBatisDemoApplication.class, args);
	}

}
